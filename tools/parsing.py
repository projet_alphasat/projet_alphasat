#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO

def parsing(entree,sortie1,sortie2):
	param = "chr1"
	for ligne in SeqIO.parse(entree,"fasta"):
		if param in str(ligne.id):
			#sortie1.write(str(ligne.description)+"\n")
			#sequence = str(ligne.seq)
			#print(sequence)
			SeqIO.write(ligne,sortie1,"fasta")
		else:
			#sortie2.write(str(ligne.description)+"\n")
			SeqIO.write(ligne,sortie2,"fasta")
			

monFichierEntree = open("Homo_sapiens_monomers.fst","r")
monFichierSortie1 = open("Hs_monomers_set1.fst","w")
monFichierSortie2 = open("Hs_monomers_set2.fst","w")

try:
	parsing(monFichierEntree,monFichierSortie1,monFichierSortie2)
finally:
	monFichierEntree.close()
	monFichierSortie1.close()
	monFichierSortie2.close()
