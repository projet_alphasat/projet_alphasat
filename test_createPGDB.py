#!/usr/bin/env python3
#-*- coding:Utf-8 -*-
from Bio import SeqIO
import io
import time 
import sys
import argparse
import re
import psycopg2
import psycopg2.extras
from parsing_fonctions import (memory_usage, absolue_location, insert_chromosome_chunk)
import subprocess
try:
	conn = psycopg2.connect(database="alphasats", user="alpha", password="alpha")
except:
	print ("I am unable to connect to the database")
cursor = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)


def clean_banque(banque):
	"""clean all the Collections - drop the database"""
	banque = banque.lower()
	if banque == 'y':
		cursor.execute("SELECT tablename FROM pg_tables WHERE tablename !~ '^pg_' AND tablename !~ '^sql_';")
		listeTables = list()
		for row in cursor:
			for element in row:
				listeTables.append(element)
				print (str(element))
		cursor.execute('commit')
		for i in listeTables:
			#sql = "DROP TABLE %s CASCADE;"	
			#var = "("+i+", )" 
			#print (var)
			#cursor.execute(sql,var)
			cursor.execute("DROP TABLE "+i+" CASCADE;")
		cursor.execute("""create table Taxon(
		taxon_id serial UNIQUE, 
		taxon_name varchar(50), 
		primary key(taxon_id));""")
		cursor.execute('commit')
		cursor.execute("""create table Chromosome(
		chromosome_id serial UNIQUE,
		chromosome_name varchar(30),
		chromosome_description varchar(50), 
		primary key(chromosome_id), 
		taxon_id int,
		foreign key(taxon_id) references Taxon(taxon_id) on delete CASCADE );""")
		cursor.execute('commit')
		cursor.execute("""create table Chunk(
		chunk_id serial UNIQUE,
		chunk_begin int, 
		chunk_end int,
		chunk_sequence text,
		primary key(chunk_id));""")
		cursor.execute('commit')
		cursor.execute("""create table Block(
		block_id serial unique,
		block_name varchar(60),
		chromosome_id int,
		block_begin int, 
		block_end int, 
		block_strand int,
		primary key(block_id),
		foreign key(chromosome_id) references Chromosome(chromosome_id) on delete CASCADE);""")
		cursor.execute('commit')
		cursor.execute("""create table Monomer(
		monomer_id serial UNIQUE,
		block_id int,
		chromosome_id int,
		monomer_name varchar(60),
		monomer_begin int, 
		monomer_end int, 
		monomer_strand int, 
		monomer_sequence text,
		primary key(monomer_id),
		foreign key(chromosome_id) references Chromosome(chromosome_id) on delete CASCADE,
		foreign key(block_id) references Block(block_id) on delete CASCADE);""")
		cursor.execute('commit')
		cursor.execute("""create table HOR(
		HOR_id serial UNIQUE,
		ordre int,
		primary key(HOR_id));""")
		cursor.execute('commit')
		cursor.execute("""create table Family(
		family_id serial UNIQUE,
		family_name varchar(30),
		primary key(family_id));""")
		cursor.execute('commit')
	elif banque not in ('n','y'):
		sys.exit("Syntax Error. Please check the help option -h")

	
"""
#cursor.execute("DROP TABLE Taxon CASCADE;")
#cursor.execute("DROP TABLE Chromosome CASCADE;")
#cursor.execute("DROP TABLE Chunk CASCADE;")
#cursor.execute("DROP TABLE Block CASCADE;")
#cursor.execute("DROP TABLE Monomer CASCADE;")
#cursor.execute("DROP TABLE Family CASCADE;")
#cursor.execute("DROP TABLE HOR CASCADE;")
#cursor.execute('commit')	
"""


def insert_taxon(taxon_name):
	"""insert the taxon"""
	#MV : Fichiers de taxons?
	try:
		cursor.execute("insert into Taxon (taxon_name) values (%s) ;", (taxon_name,))
		cursor.execute('commit')
	except psycopg2.Error as e:
			print (e.pgerror)
	cursor.execute("select taxon_id from Taxon where taxon_name = %s ;", (taxon_name,))
	for row in cursor:
		for i in row:
			taxon_id = i
	cursor.execute('commit')
	return (taxon_id)

def insert_chromosome(chromosome_list, taxon_identifiant,description):
	""" insert chromsomome without their sequences.
	WARNING : the file enter cannot contain the sequence, 
	only chromosome name""" 
	chromosome_id = list()
	taxon_id = str(taxon_identifiant)
	#print (isinstance(taxon_id,int))
	for i in chromosome_list:
		try:
			cursor.execute("insert into Chromosome (chromosome_name,taxon_id) values (%s,%s);", (i,taxon_id,))
			cursor.execute('commit')
			cursor.execute("select chromosome_id from Chromosome where chromosome_name = %s ;", (i,))
			for row in cursor:
				for i in row:
					chromosome_id.append(row)
			cursor.execute('commit')
		except psycopg2.Error as e:
			print (e.pgerror)
	return chromosome_id


def gen_monomers_names(m_file):
	"""use by the generator, to get the monomers names"""
	with open(m_file) as monomer_file:
		for line in monomer_file:
			if not line.startswith('>'):
				continue
			yield line[1:-1]


def gen_monomers_dict_sequence(m_file):
	"""get a dict monomers name : sequence"""
	monomers = dict()
	for seq_record in SeqIO.parse(m_file, "fasta"):
		#Dictionnaire nom_monomere : sequence_monomere
		monomers[seq_record.id] = str(seq_record.seq)
	return monomers


def gen_monomers_docs(m_file, block_list): #, block_list2):
	t = io.StringIO()
	liste1 = []
	#liste2 = []
	""" generate the monomers docs for the generator"""
	#Dictionnaire monomeres noms : sequences
	monomers_sequence = gen_monomers_dict_sequence(m_file)
	for name in gen_monomers_names(m_file):
		parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
		if not parse:
			print ("Warning, invalid monomer %s " % name)
			continue
		#Attribution des variables a leurs valeurs provenant du nom du monomere et separe par des "_" et recupere avec parse.groups()
		(chromosome_name, block_begin, block_end, strand,monomer_begin, monomer_end) = parse.groups()
		#Obtention des positions sur le chromosome des monomeres a l'aide d'une fonction "absolue_location"
		(mono_begin, mono_end) = absolue_location(int(monomer_begin),int(monomer_end), int(block_begin), int(block_end), int(strand))
		#Nom du block
		block_name = '_'.join([chromosome_name, block_begin, block_end, strand])
		#block_list[block_name] -> recuperation de block_id pour un nom de block donne
		#monomers_sequence[name] -> recuperation de la sequence pour un nome de monomere donne
		#print("coucou")
		#print(block_list[block_name])
		#liste1.extend([name,str(mono_begin),str(mono_end),str(strand),block_list[block_name],monomers_sequence[name]])
		liste1.extend([name,str(mono_begin),str(mono_end),str(strand),str(block_list[block_name][0]),str(block_list[block_name][1]),monomers_sequence[name]])
		#liste1.append(name)
		#print("\t".join(liste1)+"\n")
		t.write("\t".join(liste1)+"\n")
		#print(t)
		#liste2.append(liste1)
		liste1 = []
	#return(liste2)
	t.seek(0)
	return(t)


def gen_infos(m_file):
	"""with the monomers names, get all the informations 
	about the monomers : begin, end, strand, block_begin, block_end, 
	chromosome"""
	for name in gen_monomers_names(m_file):
		parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$", name)
		if not parse:
			print ("Warning, invalid monomer %s " % name)
			continue
		info = dict(zip(('chromosome', 'block_begin', 'block_end', 'strand', 'monomer_begin', 'monomer_end'), parse.groups()))
		#Calcule la position genomique de debut et de fin d'un monomere
		(monomer_begin, monomer_end) = absolue_location(int(info['monomer_begin']),int(info['monomer_end']), int(info['block_begin']),int(info['block_end']),int(info['strand']))
		info['monomer_begin'] = monomer_begin
		info['monomer_end'] = monomer_end
		#print (info)
		yield info

	  
def gen_block_docs(m_file, chr_name_id):
	toto = open("toto","w")
	t2 = io.StringIO()
	liste1 = []
	#liste2 = []
	"""with the monomers informations, insert the block document """
	for monomer_info in gen_infos(m_file):
		#yield {k:v for (k, v) in monomer_info.items() if k in 
		#('chromosome', 'block_begin', 'block_end', 'strand')}
		block_begin = monomer_info['block_begin']
		block_end = monomer_info['block_end']
		strand = monomer_info['strand']
		name = '_'.join([monomer_info['chromosome'], monomer_info['block_begin'], monomer_info['block_end'], monomer_info['strand']])
		Chromosome_id = chr_name_id[monomer_info['chromosome']]
		liste1.extend([str(block_begin),str(block_end),str(strand),str(name),str(Chromosome_id)])
		#block = dict(zip(('block_begin','block_end','block_strand','block_name','chromosome_id'),(block_begin,block_end,strand,name,Chromosome_id)))
		#print("\t".join(liste1)+"\n")
		#print("toto")
		t2.write("\t".join(liste1)+"\n")
		#liste2.append(liste1)
		liste1 = []
	#return(block)
	#return(liste2)	
	view = t2.getvalue()
	toto.write(view)
	print(view)
	toto.close()
	t2.seek(0)	
	return(t2)


def create_data(m_file, taxon_id):
	"""insert blocks and monomers informations with bulk insert"""
	chromosome_id_taxon = list()
	chr_name_id = dict()
	cursor.execute("select chromosome_id from Chromosome where taxon_id = %s ;", (taxon_id,))
	for row in cursor:
		for i in row:
			chromosome_id_taxon.append(i)
	cursor.execute('commit')
	cursor.execute("select chromosome_name, chromosome_id from Chromosome where taxon_id = %s ;", (taxon_id,))
	for row in cursor:
		chr_name_id[row[0]] = row[1]
	cursor.execute('commit')
	#f = io.StringIO()
	#f2 = io.StringIO()
	gen_block_docs(m_file,chr_name_id)
	toto = open("toto","r")
	columns=['block_begin','block_end','block_strand','block_name','chromosome_id']
	coltxt = ' (%s)' % ', '.join(columns) if columns else ''
	try:
		with open(toto) as f:
			subprocess.check_call([
				'psql',
				'-c', 'COPY %s%s FROM STDIN' % ('Block', coltxt),
				'--set=ON_ERROR_STOP=true' # to be safe
			], stdin=f)
		#sql = 'COPY (%s) from STDIN'%(gen_block_docs(m_file,chr_name_id),)
		#cursor.copy_expert( sql, f,  )
		#cursor.copy_from(gen_block_docs(m_file,chr_name_id),'Block',columns=('block_begin','block_end','block_strand','block_name','chromosome_id'))
		#cursor.copy_from(toto,'Block',columns=('block_begin','block_end','block_strand','block_name','chromosome_id'))
		#cursor.copy_from(f,'Block',columns=('block_begin','block_end','block_strand','block_name','chromosome_id'))
	except psycopg2.Error as e:
		print (e.pgerror)
	#try:
		#cursor.execute("insert into Block (block_begin, block_end, block_strand, block_name, chromosome_id) values ('" + block_begin,block_end,block_strand,block_name,chromosome_id + "');")
		#cursor.execute('commit')
	#except psycopg2.Error as e:
		#print (e.pgerror)
	block_list = dict() 
	for chr_id in chromosome_id_taxon:
		#cursor.execute("select * from Block where chromosome_id = '" + str(chr_id) + "';")
		cursor.execute("select * from Block;")
		#print("tata")
		for row in cursor:
			#print("aaa")
			#print(row)
			#Dictionnaire nom_block : id_block (initialement nom_block : autres infos block, pour recup monomer_id et chromosome_id)
			block_list[row[1]] = row[0],row[2]
			#block_list2[row[1]] = row[2]
	cursor.execute('commit')
	print(memory_usage())
	try:
		#sql = 'COPY (%s) from STDIN'%(gen_monomers_docs(m_file,block_list),)
		#cursor.copy_expert( sql, f2,  )
		#cursor.copy_from(f2,'Monomer',columns=('monomer_name','monomer_begin','monomer_end','monomer_strand','block_id','chromosome_id','monomer_sequence'))
		cursor.copy_from(gen_monomers_docs(m_file,block_list),'Monomer',columns=('monomer_name','monomer_begin','monomer_end','monomer_strand','block_id','chromosome_id','monomer_sequence'))
		#cursor.copy_from(gen_monomers_docs(m_file,block_list),'Monomer',columns=('monomer_name'))
	except psycopg2.Error as e:
		print (e.pgerror)
	#try:
		#cursor.execute("insert into Monomer (monomer_name,monomer_begin,monomer_end,monomer_strand,block_id,monomer_sequence) values ('" + str(gen_monomers_docs(m_file, block_list)) + "');")
		#cursor.execute('commit')
	#except psycopg2.Error as e:
		#print (e.pgerror)	  

def monomer_mode(c_file, m_file, k_option, taxon_id):
	"""'mode' activate to enter blocks, monomers, chromosomes and taxons
	informations"""
	chr_name = list()
	chr_description = dict()
	k_option = k_option.lower()
	if k_option == 'y':
		for seq_record in SeqIO.parse(c_file, "fasta"):
			chromosomeid[seq_record.id] = insert_chromosome_chunk(
			str(seq_record.id), taxon_id, str(seq_record.description), str(seq_record.seq).upper())
			create_data(m_file, taxon_id)
	else : 
		with open(c_file, "r") as read_file:
			for line in read_file:
				new_line = line.split(">")[-1].replace("\n", "")
				if " " in new_line:
					(chrname, chr_descr) = new_line.split(" ", 1)
					chr_name.append(chrname)
					chr_description[chrname] = chr_descr
				else:
					chr_name.append(new_line)
					chr_description[new_line] = new_line
	insert_chromosome(chr_name, taxon_id,chr_description)
	create_data(m_file, taxon_id)


TPS1 = time.time()  
TPC1 = time.clock()
extended_help = """Several modes:

    MONOMER MODE:
    ============
    
    BEWARE: in this mode, if you choose -clean Y you clean the entiere database

    For this mode, -t -c -m -k -clean options are required
    This mode can be associated with Family mode and BigFamily mode
    
    eg:
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -k N -clean Y
    to enter a taxon name : hg19, clean the data base but not entered the sequences associated
    
    FAMILY MODE:
    ===========
    
    BEWARE : if clean option is choose in this mode, only the monomers' families
    dependences are removed
    
    For this mode, -t -f -clean options are required
    This mode can be associated with Monomer mode and BigFamily mode 
    
    eg:
    -t hg19 -f family_data.fst -clean N
    
    Here, you enter families data but you keep the informations 
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -clean Y
    
    If you want to clean the database you have to enter chromosome data 
    and family data
    
    BIGFAMILY MODE:
    ==============
    
    BEWARE : if clean option is choose in this mode, the entiere family data
    are removed and the monomers' families dependances
    
    For this mode, -t -clean options are required
    This mode can be associated with Family mode and Monomer/Family mode
    
    eg:
    -t hg19 -F bigfamily_data.fst -clean N
    
    Here, you enter bigfamilies data but you keep the informations and the families data
    are updated 
    
    -t hg19 -f family_data.fst -F bigfamily_data.fst -clean N
    
    Here, you keep monomers' and chromosomes' informations and you add 
    families and bigfamilies informations. The monomers' informations are
    updated
    
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family-data.fst -F bigfamily_data.fst-clean Y
    
    Here, you create en entiere new base with all the informations
    
    CYTOBAND MODE:
    ==============
    
    For this mode, -t -clean options are required
    This mode can be associated with Family mode and Monomer/Family/BigFamily mode
    
    Here, you keep monomers', chromosomes' and families' informations 
    and you add cytobands informations. The chromosomes' informations are
    updated
    
    e.g :
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -f family_data.fst -F bigfamily_data.fst -cy cytoband_data.fst -k N -clean Y
    
    Here, you create en entiere new base with all the informations
    
    -t hg19 -cy cytoband_data.fst -k N -clean Y
    
    Here, you clean the chromosomes' informations about the cytoband and update with new one
    
    -t hg19 -c chromosome_data.fst -m monomer_data.fst -cy cytoband_data.fst -clean Y
    """
 
parser = argparse.ArgumentParser(
epilog = extended_help, formatter_class = argparse.RawTextHelpFormatter)
parser.add_argument("-t", help="taxon name", required=True, type=str)
parser.add_argument("-c", help="path chromosomes' file", type=str)
parser.add_argument("-m", help="path monomers' file",  type=str)
parser.add_argument("-f", help="path family file", type=str)
parser.add_argument("-F", help="path familyparents file", type=str)
parser.add_argument("-cy", help="path cytoband file", type=str)
parser.add_argument("-clean", help="the database will be totally cleaned, Y/N",required=True, type=str)
parser.add_argument("-k", help="the sequence will be entered or not Y/N", type=str)

args = parser.parse_args()

if args.m and args.c and args.k and args.F is None and args.f is None \
and args.cy is None:
	print("Monomer Mode")
	print(memory_usage())
	clean_banque(args.clean)
	TAXON_ID = insert_taxon(args.t)
	print("New Taxon created")
	monomer_mode(args.c, args.m, args.k, TAXON_ID)
	TPS2 = time.time() 
	print("temps s %f" %(TPS2 - TPS1))
	TPC2 = time.clock()
	print("temps CPU final %f" %(TPC2-TPC1))
	sys.exit(0)
	
