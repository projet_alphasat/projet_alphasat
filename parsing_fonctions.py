
#-*- coding:Utf-8 -*-

def memory_usage():
    """Memory usage of the current process in kilobytes."""
    status = None
    result = {'peak': 0, 'rss': 0}
    try:
        # This will only work on systems with a /proc file system
        # (like Linux).
        status = open('/proc/self/status')
        for line in status:
            parts = line.split()
            key = parts[0][2:-1].lower()
            if key in result:
                result[key] = int(parts[1])
    finally:
        if status is not None:
            status.close()
    return result

def absolue_location(monomer_begin, monomer_end, block_begin, block_end,
    strand):
    """get absolus monomers positions on the chromosome and on the
    strand 1 (sens)
    exemple : NT_011630.15_230270_262823_1:30599-30769
    begin = 260869
    end = 261039
    """
    block_begin = int(block_begin)
    block_end = int(block_end)
    strand = int(strand)
    if strand == 1:
        monomer_begin_abs = block_begin+ monomer_begin
        monomer_end_abs = block_begin + monomer_end
    else:
        monomer_begin_abs = block_end - monomer_end
        monomer_end_abs = block_end - monomer_begin
    return (monomer_begin_abs, monomer_end_abs)
    
def verif_integrity_chromosome(begin, end, chromosome):
    """verifie que le chromosome entré sous forme de chunks dans
    la base est bien intègre une fois reconstitué"""
    print(begin)
    print(end)
    sequence_chunks = chromosome.getSubSequence(begin, end)
    sequence_chunks = sequence_chunks.upper()
    with open("testsequence.fst", "w") as seq_file:
        seq_file.write(sequence_chunks)
        
    with open(args.c, "r") as read_file:
        read_file.readline()
        sequencechromosome = read_file.read()
        sequencechromosome = sequencechromosome.replace("\n", "")
        sequencechromosome = sequencechromosome[begin:end]
    with open("test2.fst", "w") as seq_file2:
        seq_file2.write(sequencechromosome)
    if sequencechromosome != sequence_chunks:
        sys.exit("integrity problem with chromosome")


def insert_chromosome_chunk(chromosome_name, taxon_id, description, 
    sequence):
    """option to insert chromosomes with their sequences"""
    #version avec Chunk => à priori non utilisée
    chromosome_document = {
                    'name': chromosome_name,
                    'Taxon_id': taxon_id,
                    'Description' : description
                }
    chromosome = Chromosome(document = chromosome_document)
    chromosome_id = chromosome.insert()
    chromosome.setSequence(sequence)
    chromosome.save()
    return chromosome_id

def verif_integrity_monomer(m_file, chromosome):
    #à refaire
    """check monomer_integrity => but only with chromosomes chunks option"""
    for seq_record in SeqIO.parse(m_file, "fasta"):
        parse = re.match(r"^(.*)_(\d+)_(\d+)_(-1|1):(\d+)-(\d+)$",
        seq_record.id)
        if not parse:
            print ("Warning, invalid monomer %s " % seq_record.id)
            continue
        info = dict(zip(('chromosome', 'block_begin', 'block_end', 
        'strand', 'monomer_begin', 'monomer_end'), parse.groups()))
        sequence_in_chr = chromosome.getSubSequence(
        info['begin'], info['end'])
        seqf = sequence_in_chr.replace("\n", "")
        if info['strand'] == -1:
            seq_record.seq = seq_record.seq.reverse_complement()
        if str(seq_record.seq) != seqf:
            sys.exit("integrity monomers problem")
